resource "aws_instance" "web-server" {
  ami           = var.ami
  instance_type = var.instance_type
  key_name      = var.ssh_key
  tags = {
    Name    = var.instance_name
    Env     = var.environment
    Product = var.product
  }
  provisioner "remote-exec" {
  inline = ["echo 'Checking SSH connectivity'"]
    connection {
        type = "ssh"
        user = "centos"
        private_key = file("/var/lib/jenkins/key.pem")
        host = aws_instance.web-server.public_ip
    }
  }
  provisioner "local-exec" {
    command = "ansible-playbook -i ${aws_instance.web-server.public_ip}, --private-key /var/lib/jenkins/key.pem ../modules/ec2-module/apache.yaml"
  }
}
