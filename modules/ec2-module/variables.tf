variable "instance_type" {
  type = string
}

variable "ami" {
  type = string
}

variable "ssh_key" {
  type = string
}

variable "instance_name" {
  type = string
}

variable "environment" {
  type = string
}

variable "product" {
  type = string
}

variable "s3bucketkey" {
  type = string
}